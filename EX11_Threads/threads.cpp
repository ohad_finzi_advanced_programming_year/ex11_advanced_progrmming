#include "threads.h"


/*
The function just prints the sentence "I love threads"
*/
void I_Love_Threads()
{
	std::cout << "I love threads" << std::endl;
}


/*
The function calls the function I_Love_Threads as a thread
*/
void call_I_Love_Threads()
{
	std::thread t(I_Love_Threads);
	t.join();
}


/*
The function prints the whole given vector elements
*/
void printVector(std::vector<int> primes)
{
	int i = 0;

	for (i = 0; i < primes.size(); i++)  //runs on the vector size
	{
		std::cout << primes[i] << std::endl;  //prints the current vector element
	}
}


/*
The function calculates all the prime number that are between the two given numbers, and puts them inside the given vector
Input: int begin and int end which symbolizes the range of numbers to search for primes, and the vector to put all the primes numbers in
*/
void getPrimes(int begin, int end, std::vector<int>& primes)
{
	int i = 0;
	int j = 0;
	bool isPrime = true;

	for (i = begin; i <= end; i++)  //a loop which runs on the the given number range
	{
		isPrime = true;  //resets the check if prime element
		for (j = 2; j <= i / 2 && isPrime; j++)  //runs from the 2 to half of the current checkd number
		{
			if (i % j == 0)  //if the current checkd number can be divided by j, it means that is he is not a prime number
			{
				isPrime = false;  //breaks the loop
			}
		}

		if (isPrime)  //if the number current num is a prime number, it will get pushed to the given vector
		{
			primes.push_back(i);
		}
	}
}


/*
The function call the getPrimes functon as a thread and calculates the time it took to finish
Input: int begin and int end which symbolizes the range of numbers to search for primes
Output: a vector with only the prime numbers between the two given numbers

used the link below to find the information about the time.h library:
https://levelup.gitconnected.com/8-ways-to-measure-execution-time-in-c-c-48634458d0f9
*/
std::vector<int> callGetPrimes(int begin, int end)
{
	std::vector<int> primes;
	clock_t beginClock, endClock;
	double elapsed = 0;

	beginClock = clock();  //gets the current time (begin of thread)
	std::thread t(getPrimes, begin, end, std::ref(primes));  //activates the getPrimes function as a thread
	t.join();  //waits for the thread to end
	endClock = clock();  //gets the current time (end of thread)

	elapsed = double(endClock - beginClock) / CLOCKS_PER_SEC;  //calculates the time it took for the thread to be over
	std::cout << " GetPrimes thread exacution time: " << elapsed << std::endl;  //and prints it

	return primes;
}


/*
The function writes to a file all the primes numbers in the range of the two given numbers
Input: int begin and int end which symbolizes the range of numbers to search for primes and an open file to write the range of all the prime numbers in the range of the two given numbers
*/
void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	int i = 0;
	int j = 0;
	bool isPrime = true;

	for (i = begin; i <= end; i++)  //a loop which runs on the the given number range
	{
		isPrime = true;  //resets the check if prime element
		for (j = 2; j <= i / 2 && isPrime; j++)  //runs from the 2 to half of the current checkd number
		{
			if (i % j == 0)  //if the current checkd number can be divided by j, it means that is he is not a prime number
			{
				isPrime = false;  //breaks the loop
			}
		}

		if (isPrime)  //if the number current num is a prime number, it will get written to the given file
		{
			file << i;
			file << '\n';
		}	
	}
}


//with using the callGetPrimes function from the other exercise
///*
//The function writes to a file all the primes numbers in the range of the two given numbers
//Input: int begin and int end which symbolizes the range of numbers to search for primes and an open file to write the range of all the prime numbers in the range of the two given numbers
//*/
//void writePrimesToFile(int begin, int end, std::ofstream& file)
//{
//	int i = 0;
//	std::vector<int> primes = callGetPrimes(begin, end);  //activates the callGetPrimes function as thread with the two given numbers
//
//	for (i = 0; i < primes.size(); i++)  //writes each element of the vector with all of the  prime numbers to the given file
//	{
//		file << primes[i];
//		file << '\n';
//	}
//}


/*
The function creates a several threads (according to the given N integer)and each calls the function WritePrimesToFile
Input: int begin and int end which symbolizes the range of numbers to search for primes, int N which syblozies the number of threads to create, and the file path to put all the prime numbers in
*/
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream file(filePath);

	clock_t beginClock, endClock;
	double elapsed = 0;

	int range = (end - begin) / N;
	int newBegin = begin;
	int newEnd = begin;
	int i = 0;

	if (file.is_open())  //will create the threads only if the given path is valid
	{
		beginClock = clock();  //gets the current time (begin of all threads)

		for (i = 0; i < N; i++)
		{
			newEnd = newBegin + range;  //adds the range (calculated at the start) to the begin value
			std::thread t(writePrimesToFile, newBegin, newEnd, std::ref(file));  //creates a thread which activates the writePrimesToFile function with the correct elements
			newBegin = newEnd;  //sets the begin value to the end value for the next thread
			t.join();  //waits for the thread to end its run
		}

		endClock = clock();  //gets the current time (end of all threads)

		elapsed = double(endClock - beginClock) / CLOCKS_PER_SEC;  //calculates the time it took for all the threads to be over
		std::cout << "All threads exacution time: " << elapsed << std::endl;  //and prints it

		file.close();  //closes the file to prevent memory leakage
	}
	else  //incase the file isnt valid
	{
		std::cerr << "Unable to open file" << std::endl;
		exit(-1);
	}
}


/*
The function creates threads until the given number reaches 50
*/
void bonus3(int num)
{
	if (num <= MAX_THREAD_NUM)
	{
		std::cout << "Hello from Thread " << num << std::endl;
		std::thread t(bonus3, num + 1);
		t.join();
	}
}

